﻿namespace SocketDummy.Models
{
    public class SocketValueChannel : SocketFMU_Generator.Models.SocketChannel
    {
        private double _value;
        public double Value
        {
            get
            {
                return _value;
            }
            set
            {
                Set(ref _value, value);
            }
        }
    }
}
