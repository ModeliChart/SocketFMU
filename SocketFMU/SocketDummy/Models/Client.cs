﻿using SocketFMU_Generator.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace SocketDummy.Models
{
    public class Client : GalaSoft.MvvmLight.ObservableObject
    {
        private TcpClient _client;
        private CancellationTokenSource _stopClientSource;

        private int _port;
        public int Port
        {
            get
            {
                return _port;
            }
            set
            {
                Set(ref _port, value);
            }
        }
        private ObservableCollection<SocketValueChannel> _channels;
        public ObservableCollection<SocketValueChannel> Channels
        {
            get
            {
                return _channels;
            }
            set
            {
                Set(ref _channels, value);
            }
        }

        public Client()
        {
            _channels = new ObservableCollection<SocketValueChannel>();
        }
        public Client(string path) : this()
        {
            SetupFromFmu(path);
        }

        /// <summary>
        /// Creates the Channels which can contain values and implement INotifyPropertyChanged.
        /// Also sets the port.
        /// </summary>
        /// <param name="path">The full path to the fmu file.</param>
        public void SetupFromFmu(string path)
        {
            // Setup timestamp channel
            var timestampChannel = new SocketValueChannel()
            {
                Name = "Timestamp",
                DisplayedUnit = "Seconds",
                IsSender = true,
                Value = 0
            };
            timestampChannel.PropertyChanged += SocketValueChannel_PropertyChanged;
            _channels.Add(timestampChannel);

            // Load the Json description from the fmu
            var description = FmuLoader.LoadFmu(path);

            foreach (SocketChannel channel in description.Channels)
            {
                var socketValueChannel = new SocketValueChannel()
                {
                    Name = channel.Name,
                    DisplayedUnit = channel.DisplayedUnit,
                    IsSender = channel.IsSender
                };
                // Subscripe propertyChanged event to register value changes
                socketValueChannel.PropertyChanged += SocketValueChannel_PropertyChanged;
                _channels.Add(socketValueChannel);
            }

            Port = description.ReceivePort;
        }

        private void SocketValueChannel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // If a value has changed send all values to the server

            if (e.PropertyName == nameof(SocketValueChannel.Value))
            {
                // Create buffer in NetworOrder (BigEndian)
                var buffer = new System.Collections.Generic.List<byte>();

                // Gather data from all channels except the timestamp
                for (int i = 1; i < Channels.Count; i++)
                {
                    byte[] bytes = BitConverter.GetBytes(Channels[i].Value);
                    if (BitConverter.IsLittleEndian)
                    {
                        bytes = bytes.Reverse().ToArray();
                    }
                    buffer.AddRange(bytes);
                }

                // Send all the values
                _client.GetStream().Write(buffer.ToArray(), 0, buffer.Count);
            }
        }

        public async Task StartClient()
        {
            try
            {
                _client = new TcpClient();
                _client.Connect("localhost", Port);

                _stopClientSource = new CancellationTokenSource();
                while (!_stopClientSource.IsCancellationRequested && _client.Connected)
                {
                    // Receive timestamps and all channels: [Timestamp, Channel 1, Channel 2, ..., Channel n]
                    byte[] buffer = new byte[(Channels.Count) * sizeof(double)];
                    await _client.GetStream().ReadAsync(buffer, 0, buffer.Length);

                    // Split the buffer into double sized bytes -> endianness -> byte to double array -> set value
                    for (int i = 0; i < Channels.Count; i++)
                    {
                        // Receiver Channels shall only be edited by the user input
                        if (Channels[i].IsSender)
                        {
                            // Extract the bytes for one double, 
                            byte[] doubleBuffer = new byte[sizeof(double)];
                            Array.Copy(buffer, i * sizeof(double), doubleBuffer, 0, sizeof(double));
                            // Endianness
                            if (BitConverter.IsLittleEndian)
                            {
                                // Network to host
                                var littleEndianBuffer = doubleBuffer.Reverse().ToArray();
                                Channels[i].Value = BitConverter.ToDouble(littleEndianBuffer, 0);
                            }
                            else
                            {
                                Channels[i].Value = BitConverter.ToDouble(doubleBuffer, 0);
                            }
                        }
                    }
                }
                // Client has been cancelled
                _client.GetStream().Close();
                _client.Close();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        public void StopClient()
        {
            _stopClientSource.Cancel();
        }
    }
}
