using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;
using SocketDummy.Models;
using Microsoft.Win32;

namespace SocketDummy.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private Client _client;

        public ObservableCollection<SocketValueChannel> SocketChannels
        {
            get
            {
                return _client.Channels;
            }
        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            _client = new Client();
        }

        private RelayCommand _loadFmu;
        public RelayCommand LoadFmu
        {
            get
            {
                return _loadFmu ?? (_loadFmu = new RelayCommand(() =>
                {
                    // Determine filename
                    var dialog = new OpenFileDialog();
                    dialog.DefaultExt = "fmu";
                    dialog.Filter = "Functional Mock-up Interface files (*.fmu) | *.fmu";
                    dialog.RestoreDirectory = true;
                    dialog.ShowDialog();
                    // Setup from fmu
                    _client.SetupFromFmu(dialog.FileName);
                }));
            }
        }

        private RelayCommand _startClient;
        public RelayCommand StartClient
        {
            get
            {
                return _startClient ?? (_startClient = new RelayCommand(async () =>
                {
                    await _client.StartClient();
                }));
            }
        }

        private RelayCommand _stopClient;
        public RelayCommand StopClient
        {
            get
            {
                return _stopClient ?? (_stopClient = new RelayCommand(() =>
                {
                    _client.StopClient();
                }));
            }
        }
    }
}