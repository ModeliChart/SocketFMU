﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Win32;
using SocketFMU_Generator.Models;
using System.Collections.ObjectModel;

namespace SocketFMU_Generator.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        // Vars
        private ObservableCollection<SocketChannel> _socketChannels;
        public ObservableCollection<SocketChannel> SocketChannels
        {
            get
            {
                return _socketChannels;
            }
            set
            {
                // Assign the valueRefs
                for(int i = 0; i < value.Count; i++)
                {
                    value[i].ValueRef = (uint)i;
                }
                Set(ref _socketChannels, value);
            }
        }

        private int _reveivePort;
        public int ReceivePort
        {
            get
            {
                return _reveivePort;
            }
            set
            {
                Set(ref _reveivePort, value);
            }
        }

        private int _sendPort;
        public int SendPort
        {
            get
            {
                return _sendPort;
            }
            set
            {
                Set(ref _sendPort, value);
            }
        }

        private ObservableCollection<string> _protocolList;
        public ObservableCollection<string> ProtocolList
        {
            get => _protocolList ?? (_protocolList = new ObservableCollection<string>() { "TCP", "UDP" });
        }

        private string _protocol;
        public string Protocol
        {
            get => _protocol;
            set => Set(ref _protocol, value);
        }

        private string _remoteAddress = "localhost";
        public string RemoteAddress
        {
            get => _remoteAddress;
            set => Set(ref _remoteAddress, value);
        }

        private string _guid;
        public string Guid
        {
            get
            {
                return (_guid);
            }
            set
            {
                Set(ref _guid, value);
            }
        }

        // Constructor
        public MainViewModel()
        {
            // Create GUID
            _guid = System.Guid.NewGuid().ToString();
            _socketChannels = new ObservableCollection<SocketChannel>();
        }

        // Commands
        private RelayCommand _saveFmu;
        public RelayCommand SaveFmu
        {
            get
            {
                return _saveFmu ??
                    (_saveFmu = new RelayCommand(() =>
                    {
                        // Determine filename
                        var dialog = new SaveFileDialog();
                        dialog.DefaultExt = "fmu";
                        dialog.Filter = "Functional Mock-up Interface files (*.fmu) | *.fmu";
                        dialog.RestoreDirectory = true;
                        dialog.ShowDialog();

                        // Create description
                        var description = new JsonModelDescription()
                        {
                            Channels = this.SocketChannels,
                            Guid = this.Guid,
                            RemoteAddress = this.RemoteAddress,
                            ReceivePort = this.ReceivePort,
                            SendPort = this.SendPort,
                            Protocol = this.Protocol
                        };

                        FmuWriter.WriteFmu(dialog.FileName, description);
                    }));
            }
        }

        private RelayCommand _loadFmu;
        public RelayCommand LoadFmu
        {
            get
            {
                return _loadFmu ??
                    (_loadFmu = new RelayCommand(() =>
                {
                    // Determine filename
                    var dialog = new OpenFileDialog();
                    dialog.DefaultExt = "fmu";
                    dialog.Filter = "Functional Mock-up Interface files (*.fmu) | *.fmu";
                    dialog.RestoreDirectory = true;
                    dialog.ShowDialog();

                    // Load description
                    var description = FmuLoader.LoadFmu(dialog.FileName);
                    if (description != null)
                    {
                        this.SocketChannels = new ObservableCollection<SocketChannel>(description.Channels);
                        this.Guid = description.Guid;
                        this.ReceivePort = description.ReceivePort;
                        this.SendPort = description.SendPort;
                        this.Protocol = description.Protocol;
                    }
                }));
            }
        }
    }
}
