﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Windows;
using System.Xml.Serialization;

namespace SocketFMU_Generator.Models
{
    public class FmuWriter
    {
        private const string MODEL_NAME = "SocketFmu";

        /// <summary>
        /// Write the model to an fmu file.
        /// </summary>
        /// <param name="filename">Complete path + filename of the fmu.</param>
        /// <param name="description">Used to create modelDescription.xml and modelDescription.json</param>
        public static void WriteFmu(string filename, JsonModelDescription description)
        {
            string tempPath = "FmuWrite";

            try
            {
                createFolders(tempPath);

                // The json for the binary and the client
                writeJsonModelDescription(description, getResourcesPath(tempPath));

                // The modeldescription of the fmi standard
                writeFmiModelDescription(description, getRootPath(tempPath));

                // Write the binary
                writeDll(getBinariesPath(tempPath));


                // Zip it
                if (!string.IsNullOrWhiteSpace(filename))
                {
                    if (File.Exists(filename))
                    {

                        File.Delete(filename);

                    }
                    // Encode with "/" instead of "\\" so we are linux compatible
                    System.IO.Compression.ZipFile.CreateFromDirectory(getRootPath(tempPath), filename, CompressionLevel.Optimal, false, new MyEncoder());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                // Cleanup
                Directory.Delete(tempPath, true);
            }
        }

        /// <summary>
        /// Create config file and xmlFile for the given data
        /// </summary>
        /// <param name="description">The whole data to be saved</param>
        /// <param name="rootPath">Root of the folder which will be zipped to an fmu.</param>
        private static void writeFmiModelDescription(JsonModelDescription description, string rootPath)
        {
            // Basic information
            var fmd = createModelDescription(description);

            // Create collection of ScalarVariables
            List<fmi2ScalarVariable> variables = new List<fmi2ScalarVariable>();
            foreach (SocketChannel channel in description.Channels)
            {
                // General variable description
                fmi2ScalarVariable variable = new fmi2ScalarVariable();
                variable.name = channel.Name;
                variable.valueReference = (uint)variables.Count;
                variable.initial = fmi2ScalarVariableInitial.exact;
                if (channel.IsSender)
                {
                    // Sender will be input (settable) of fmu
                    variable.description = "Sender channel";
                    variable.causality = fmi2ScalarVariableCausality.input;
                    variable.variability = fmi2ScalarVariableVariability.tunable;
                }
                else
                {
                    // Receiver will be output (non settable) of fmu
                    variable.description = "Receiver channel";
                    variable.causality = fmi2ScalarVariableCausality.output;
                    variable.variability = fmi2ScalarVariableVariability.discrete;
                }

                // Specify realvariable attributes
                fmi2ScalarVariableReal realvariable = new fmi2ScalarVariableReal();
                realvariable.unit = channel.DisplayedUnit;
                realvariable.displayUnit = channel.DisplayedUnit;
                realvariable.maxSpecified = false;
                realvariable.minSpecified = false;

                // Add variable to list
                variable.Item = realvariable;
                variables.Add(variable);
            }

            // Apply the variables to the model
            fmiModelDescriptionModelVariables vars = new fmiModelDescriptionModelVariables();
            vars.ScalarVariable = variables.ToArray();
            fmd.ModelVariables = vars;

            // Serialize the xml
            using (TextWriter writer = new StreamWriter(Path.Combine(rootPath, "modelDescription.xml"), false))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(fmiModelDescription));
                serializer.Serialize(writer, fmd);
            }
        }
        /// <summary>
        /// Creates the basic information and description, used by writeFmiWriteModelDescription
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        private static fmiModelDescription createModelDescription(JsonModelDescription description)
        {
            fmiModelDescription fmd = new fmiModelDescription();
            fmd.modelName = MODEL_NAME;
            fmd.author = "Tim Übelhör";
            fmd.description = "FMU which allows interaction via sockets. The protocol is: " + description.Protocol +
                " Listens to port: " + description.ReceivePort + " Sends to port: " + description.SendPort;
            fmd.fmiVersion = "2.0";
            fmd.generationDateAndTime = DateTime.UtcNow;
            fmd.generationTool = "SocketFmu_Generator";
            fmiModelDescriptionCoSimulation[] coSimulation = new fmiModelDescriptionCoSimulation[1];
            coSimulation[0] = new fmiModelDescriptionCoSimulation();
            coSimulation[0].modelIdentifier = "SocketFmu";
            coSimulation[0].canHandleVariableCommunicationStepSize = true;
            coSimulation[0].needsExecutionTool = false;
            fmd.CoSimulation = coSimulation;
            fmd.guid = description.Guid;

            return fmd;
        }
        /// <summary>
        /// Write the binary to the given path
        /// </summary>
        /// <param name="dlldDir">The path where the dll will be saved</param>
        /// <param name="useDebugDll">When checked the debug version of the dl will be used </param>
        private static void writeDll(string binPath)
        {
            // Create dirs
            Directory.CreateDirectory(binPath + "/win64");
            Directory.CreateDirectory(binPath + "/linux64");

            // Copy binaries
            File.Copy("SocketFMU.dll", binPath + "/win64/" + MODEL_NAME + ".dll");
            File.Copy("SocketFmuLinux.so", binPath + "/linux64/" + MODEL_NAME + ".so");
        }

        /// <summary>
        /// Writes the description into a json file.
        /// The file can be used by the dll, the client and this application.
        /// </summary>
        /// <param name="guid">Unique identifier</param>
        /// <param name="port">The server will listen for connections on this port.</param>
        /// <param name="channels">Enumerable collection of SocketChannels.</param>
        /// <param name="resourcePath">The path where the json will be written.</param>
        private static void writeJsonModelDescription(JsonModelDescription description, string resourcePath)
        {
            string output = JsonConvert.SerializeObject(description, Formatting.Indented);
            using (TextWriter writer = new StreamWriter(Path.Combine(resourcePath, "modelDescription.json"), false))
            {
                writer.Write(output.ToString());
            }
        }

        private static void createFolders(string tempPath)
        {
            if (File.Exists(tempPath))
            {
                File.Delete(tempPath);
            }
            // Will create all subdirectories
            Directory.CreateDirectory(getRootPath(tempPath));
            Directory.CreateDirectory(getResourcesPath(tempPath));
        }

        private static string getRootPath(string tempPath)
        {
            return Path.Combine(tempPath, "SocketFmu");
        }

        private static string getResourcesPath(string tempPath)
        {
            return Path.Combine(getRootPath(tempPath), "resources");
        }

        private static string getBinariesPath(string tempPath)
        {
            return Path.Combine(getRootPath(tempPath), "binaries");
        }
    }

    class MyEncoder : UTF8Encoding
    {
        public MyEncoder()
        {

        }
        public override byte[] GetBytes(string s)
        {
            s = s.Replace("\\", "/");
            return base.GetBytes(s);
        }
    }
}
