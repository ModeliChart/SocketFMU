﻿using System.IO;
using System.IO.Compression;
using Newtonsoft.Json;

namespace SocketFMU_Generator.Models
{
    public class FmuLoader
    {
        /// <summary>
        /// Extracts the modelDescription from the fmu
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static JsonModelDescription LoadFmu(string path)
        {
            JsonModelDescription description = null;
            try
            {
                ZipFile.ExtractToDirectory(path, "FmuLoad");

                // Load File
                string jsonPath = "FmuLoad/resources/modelDescription.json";
                string content;
                using (TextReader reader = new StreamReader(jsonPath, false))
                {
                    content = reader.ReadToEnd();
                }

                // Deserialize
                description = JsonConvert.DeserializeObject<JsonModelDescription>(content);
            }
            finally
            {
                // Remove and return
                Directory.Delete("FmuLoad", true);
            }
            return description;
        }
    }
}
