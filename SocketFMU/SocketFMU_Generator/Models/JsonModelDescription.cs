﻿using System.Collections.Generic;

namespace SocketFMU_Generator.Models
{
    public class JsonModelDescription
    {
        public string Guid
        {
            get;
            set;
        }


        public string Protocol
        {
            get;
            set;
        }

        public string RemoteAddress
        {
            get;
            set;
        }

        public int ReceivePort
        {
            get;
            set;
        }

        public int SendPort
        {
            get;
            set;
        }

        public IEnumerable<SocketChannel> Channels
        {
            get;
            set;
        }
    }
}
