﻿namespace SocketFMU_Generator.Models
{
    // TODO implement BindableBase
    public class SocketChannel: GalaSoft.MvvmLight.ObservableObject
    {
        private string _name;
        private uint _valueRef;
        private string _displayedUnit;
        private bool _isSender;
        private bool _reverseEndianness;

        /// <summary>
        /// Displayed name of the channel
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }set
            {
                Set(ref _name, value);
            }
        }

        public uint ValueRef
        {
            get
            {
                return _valueRef;
            }
            set
            {
                Set(ref _valueRef, value);
            }
        }

        /// <summary>
        /// Displayed unit of the channl
        /// </summary>
        public string DisplayedUnit
        {
            get
            {
                return _displayedUnit;
            }set
            {
                Set(ref _displayedUnit, value);
            }
        }
        /// <summary>
        /// Whether this channel is sender or receiver
        /// </summary>
        public bool IsSender
        {
            get
            {
                return _isSender;
            }set
            {
                Set(ref _isSender, value);
            }
        }

        /// <summary>
        /// Whether the channel value's endianness should be reversed
        /// </summary>
        public bool ReverseEndianness
        {
            get 
            { 
                return _reverseEndianness; 
            }
            set
            {
                Set(ref _reverseEndianness, value);
            }
        }


        public SocketChannel()
        {
        }
    }
}
