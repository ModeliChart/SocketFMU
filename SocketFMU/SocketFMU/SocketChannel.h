#pragma once
#include <string>
class SocketChannel
{
private:
	std::string _name;
	unsigned int _valueRef;
	bool _isSender;
	bool _reverseEndianness;
	double _value;
	SocketChannel();
public:
	SocketChannel(std::string name, unsigned int valueRef, bool settable, bool reverseEndianness);
	~SocketChannel();

	std::string GetName();
	unsigned int GetValueRef();
	double GetValue() const;
	void SetValue(double value);
	bool IsSender() const;
	bool ReverseEndianness() const;
};

