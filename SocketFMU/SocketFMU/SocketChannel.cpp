#include "SocketChannel.h"


SocketChannel::SocketChannel()
{
}


SocketChannel::SocketChannel(std::string name, unsigned int valueRef, bool settable, bool reverseEndianness) :
	_name(name), _valueRef(valueRef), _isSender(settable), _reverseEndianness(reverseEndianness), _value(0)
{
}

SocketChannel::~SocketChannel()
{
}

std::string SocketChannel::GetName()
{
	return _name;
}

unsigned int SocketChannel::GetValueRef()
{
	return _valueRef;
}

double SocketChannel::GetValue() const
{
	return _value;
}

void SocketChannel::SetValue(double value)
{
	_value = value;
}

bool SocketChannel::IsSender() const
{
	return _isSender;
}

bool SocketChannel::ReverseEndianness() const
{
	return _reverseEndianness;
}
