#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "fmi2Functions.h"

// Forward declarations
class ConnectionBase;
class SocketChannel;

/// Use this class for storing all data (e.g. channel values) and variables (e.g. socket for the connection)
class SocketFmiComponent
{
private:
	// General fmi information
	std::string _instanceName;
	std::string _guid;
	fmi2Status _status;
	const fmi2CallbackFunctions* _callbackFunctions;
	// Simulation time
	double _currentTime;
	// Data storage, sorted by the valueRef: 0,1,2,...
	std::vector<SocketChannel> _channels;
	std::mutex _mutex;
	// Connection
	std::unique_ptr<ConnectionBase> _connection;

	/// Will be called by the server everytime new values arrive.
	void valuesReceivedCallback(std::shared_ptr<std::vector<double>> receivedValues);
public:
	/**
	* Creates a new instance of the SocketFmiComponen
	* \param instanceName The unique identifier according to the fmi standard
	* \param xmlGuid The guid taken from the xml. Makes sure that the xml and json match.
	* \param resourceDir The path to the directory, where the modelDescription.json is located.
	* \param io_service Used by the server for async operations.
	* \callbackFunctions Used for lof and memory allocation.
	*/
	SocketFmiComponent();
	~SocketFmiComponent();

	/**
	* Creates a TcpServer object and starts listening to connections.
	* Also loads all neccessary from the json and compares if the guids match.
	* \param instanceName The unique identifier according the fmiStandard
	* \param xmlGuid The Guid which has been passed to the instantiate method and should be taken from the matching modelDescription.xml
	* \param resourceDir The directory where the resources of the FMU are stored, including the modelDescription.json
	* \param callbackFunctions The callbacks for logging and memory allocation
	*/
	fmi2Status Instantiate(std::string instanceName, std::string xmlGuid, std::string resourceDir,
		const fmi2CallbackFunctions* callbackFunctions);

	/**
	* Do one simulaton step: Send current values Increseas the current timestamp by stepSize afterwards.
	* \param currentCommunictionPoint Will be used as timestamp and compared to internal currentTime
	* \param stepSize The currentTime will be increased by this value.
	*/
	void DoStep(fmi2Real currentCommunicationPoint, fmi2Real stepSize);
	/**
	* Set tunable parameter from the FMI
	* \param vr The valuereferences.
	* \param values The values matching the vr.
	* \param nvr Size of the arrays
	*/
	void SetValues(const fmi2ValueReference vr[], const fmi2Real values[], size_t nvr);
	/**
	* Get the values matching the vr.
	* \param vr The valuereferences.
	* \param values The values matching the vr.
	* \param nvr Size of the arrays
	*/
	void GetValues(const fmi2ValueReference vr[], fmi2Real values[], size_t nvr);

	/// Returns the status of this component.
	fmi2Status GetStatus();
	/*
	* Log a status and a string for this component. Will be received by the user in the fmi2Logger
	*/
	void Log(const fmi2Status status, const std::string msg);

	/**
	* Resets all the values and connections
	*/
	void Reset();

	/**
	* Terminates receiving and sending values.
	*/
	void Terminate();
};

