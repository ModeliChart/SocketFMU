#pragma once
#include <thread>
#include <vector>
#include "boost/asio.hpp"
#include "ConnectionBase.h"

namespace asio = boost::asio;
using tcp = asio::ip::tcp;

class TcpConnection : public ConnectionBase
{
private:
	// For async exectuion
	std::thread _ioThread;
	asio::io_service _ioService;
	std::unique_ptr<asio::io_service::work> _work;

	// One acceptor for the server, accepts incoming connections
	tcp::acceptor _acceptor;

	// The socket for the connection
	tcp::socket _socket;

	/**
	* Start receiving values from  the first client.
	* \param valuesReceived A callback function which will handle the new data.
	*/
	void startReceive();
	/**
	* Callback for the async accept operation
	* \param socket Bind the socket to the handler to keep it alive until the action has finished. The socket may be used in the handler and kept alive even longer.
	*/
	void handleAccept(const boost::system::error_code & error);
	/**
	* Callback for the async write operation
	* \param buffer Bind the buffer to the handler to keep it alive until the operation has finished.
	*/
	void handleWrite(const boost::system::error_code & error, std::size_t bytes_transferred, std::shared_ptr<std::vector<double>> buffer);
	/**
	* Callback for the async read operation
	* \param buffer Bind the buffer to the handler to keep it alive until the operation has finished.
	*/
	void handleRead(const boost::system::error_code & error, std::size_t byte_transferred, std::shared_ptr<std::vector<double>> buffer);
public:
	/**
	* Create a new TcpServer which starts accepting new connections on the given port.
	* \param ioService The io_service which is used by the server and sockets.
	* \param port The server will listen on this port.
	* \param valuesReceivedCallback This callback will be called when new values have been received from the client.
	*/
	TcpConnection();
	/// Closes all the connections
	~TcpConnection();

	/**
	* Send the given values to all the connected clients.
	* \param values The values will be sent. Should be {timestamp, channels}
	*/
	fmi2Status SendValues(const std::vector<double>& values) override;

	fmi2Status Start(std::string remoteAddress, int receivePort, int sendPort) override;
	void Stop();
};

