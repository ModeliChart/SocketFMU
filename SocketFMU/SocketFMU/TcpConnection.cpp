#include "TcpConnection.h"
#include "EndianConverter.h"
#include <stdio.h>
#include <iostream>
#include <functional>


TcpConnection::TcpConnection() :
	_acceptor(_ioService), _socket(_ioService)
{
	// Add work so the ioService keeps running
	_work = std::make_unique<asio::io_service::work>(_ioService);
	// Run ioservice in different thread to stay responsive.
	_ioThread = std::thread([&] { _ioService.run(); });
}

TcpConnection::~TcpConnection()
{
	// Stop before destruction
	Stop();
}

fmi2Status TcpConnection::SendValues(const std::vector<double>& values)
{
	// Create shared_ptr to keep the parameter alive until the write has finished
	std::vector<double> buffer(values);
	// Send data from each socket in network order
	EndianConverter::ReverseEndianness(buffer);
	// Make shared so the buffer does not go out of scope until the operations have finished
	std::shared_ptr<std::vector<double>> bufferPtr = std::make_shared<std::vector<double>>(buffer);

	// Write the data
	asio::async_write(_socket, asio::buffer(*bufferPtr),
		std::bind(&TcpConnection::handleWrite, this, std::placeholders::_1, std::placeholders::_2, bufferPtr));
	return fmi2OK;
}

fmi2Status TcpConnection::Start(std::string remoteAddress, int receivePort, int sendPort)
{
	tcp::endpoint ep(tcp::v4(), receivePort);
	_acceptor.open(ep.protocol());
	_acceptor.set_option(tcp::acceptor::reuse_address(true));
	boost::system::error_code ec;
	_acceptor.bind(ep, ec);
	if (ec)
	{
		_logger(fmi2Fatal, "Error on binding the acceptor: " + std::string(ec.message().c_str()));
		return fmi2Fatal;
	}
	else
	{
		_acceptor.listen();

		// Create a smart pointer so the socket does not go out of scope
		_acceptor.async_accept(_socket, std::bind(&TcpConnection::handleAccept, this, std::placeholders::_1));
	}
	return fmi2OK;
}

void TcpConnection::Stop()
{
	// Stop everything
	_acceptor.close();
	_socket.close();
	_ioService.stop();
	// Wait for the ioTask to stop
	_work.reset();
	if (_ioThread.joinable())
	{
		_ioThread.join();
	}
}

void TcpConnection::startReceive()
{
	// Create the buffer
	std::shared_ptr<std::vector<double>> bufferPtr = std::make_shared<std::vector<double>>(_receivedValuesCount);
	asio::async_read(_socket, asio::buffer(*bufferPtr),
		std::bind(&TcpConnection::handleRead, this, std::placeholders::_1, std::placeholders::_2, bufferPtr));
}

void TcpConnection::handleAccept(const boost::system::error_code& error)
{
	if (!error)
	{
		startReceive();
	}
	else
	{
		_logger(fmi2Error, "Error on accepting a connection: " + std::string(error.message().c_str()));
	}
}

void TcpConnection::handleWrite(const boost::system::error_code& error, std::size_t bytes_transferred, std::shared_ptr<std::vector<double>> buffer)
{
	if (error)
	{
		_logger(fmi2Error, "Error on sending values: " + std::string(error.message().c_str()));
	}
}

void TcpConnection::handleRead(const boost::system::error_code& error,
	std::size_t byte_transferred, std::shared_ptr<std::vector<double>> buffer)
{
	if (!error)
	{
		// Convert to local byteorder
		EndianConverter::ReverseEndianness(*buffer);

		// Handle the data in callback
		_valuesReceivedCallback(buffer);

		// Continue receiving
		startReceive();
	}
	else
	{
		_logger(fmi2Error, "Error on reading values : " + std::string(error.message().c_str()));
	}
}