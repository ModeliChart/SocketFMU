#include "UdpConnection.h"
#include <iostream>
#include "EndianConverter.h"


UdpConnection::UdpConnection() :
	_receiveSocket(_ioService), _sendSocket(_ioService)
{
	// Run the ioService
	_work = std::make_unique<asio::io_service::work>(_ioService);
	_ioThread = std::thread([&] { _ioService.run(); });
}

UdpConnection::~UdpConnection()
{
	// Stop before destruction
	Stop();
}

fmi2Status UdpConnection::Start(std::string remoteAddress, int receivePort, int sendPort)
{
	// Connect the sender Socket to the endpoint
	_sendSocket.open(udp::v4());
	udp::resolver resolver(_ioService);
	udp::resolver::query query(udp::v4(), remoteAddress, std::to_string(sendPort));
	auto iter = resolver.resolve(query);
	boost::system::error_code connectEc;
	_sendSocket.connect(iter->endpoint(), connectEc);
	if (connectEc)
	{
		_logger(fmi2Fatal, "'Connecting' to the endpoint failed: " + std::string(connectEc.message().c_str()));
		return fmi2Fatal;
	}

	// Bind and "connect" the socket
	_receiveSocket.open(udp::v4());
	boost::system::error_code receiveEc;
	_receiveSocket.bind(udp::endpoint(udp::v4(), receivePort), receiveEc);
	if (receiveEc)
	{
		_logger(fmi2Fatal, "Binding the UDP receiver failed: " + std::string(receiveEc.message().c_str()));
		return fmi2Fatal;
	}
	// Ran to completion
	startReceive();
	return fmi2OK;
}

void UdpConnection::Stop()
{
	// Close all sockets
	_receiveSocket.close();
	_sendSocket.close();
	// Wait for the ioThread to finish
	_work.reset();
	if (_ioThread.joinable())
	{
		_ioThread.join();
	}
}

fmi2Status UdpConnection::SendValues(const std::vector<double>& values)
{
	if (_sendSocket.is_open())
	{
		// Create shared_ptr to keep the parameter alive until the write has finished
		std::vector<double> buffer(values);
		// Send data from each socket in network order
		EndianConverter::ReverseEndianness(buffer);
		// Make shared so the buffer does not go out of scope until the operations have finished
		std::shared_ptr<std::vector<double>> bufferPtr = std::make_shared<std::vector<double>>(buffer);

		// Send the data
		_sendSocket.async_send(asio::buffer(*bufferPtr), std::bind(&UdpConnection::handleSend, this, std::placeholders::_1, std::placeholders::_2, bufferPtr));
		return fmi2OK;
	}
	else
	{
		_logger(fmi2Warning, "Cannot send, the socket is closed.");
		return fmi2Warning;
	}
}

void UdpConnection::startReceive()
{
	// Keep shared pointers alive
	std::shared_ptr<std::vector<double>> bufferPtr = std::make_shared<std::vector<double>>(_receivedValuesCount);
	std::shared_ptr<udp::endpoint> endpointPtr = std::make_shared<udp::endpoint>();
	// Receive data and save the endpoint
	_receiveSocket.async_receive_from(asio::buffer(*bufferPtr), *endpointPtr,
		std::bind(&UdpConnection::handleReceive, this, std::placeholders::_1, std::placeholders::_2, bufferPtr, endpointPtr));
}

void UdpConnection::handleReceive(const boost::system::error_code & error, std::size_t byte_transferred,
	std::shared_ptr<std::vector<double>> buffer, std::shared_ptr<udp::endpoint> endpoint)
{
	if (!error)
	{
		// Convert to local byteorder
		EndianConverter::ReverseEndianness(*buffer);

		// Handle the data in callback
		_valuesReceivedCallback(buffer);

		// Continue receiving
		startReceive();
	}
	else
	{
		_logger(fmi2Error, "Error on receiving values: " + std::string(error.message().c_str()));
	}
}

void UdpConnection::handleSend(const boost::system::error_code & error, std::size_t bytes_transferred, std::shared_ptr<std::vector<double>> buffer)
{
	if (error)
	{
		// Currently no additional error handling only output it
		_logger(fmi2Error, "Error on sending values: " + std::string(error.message().c_str()));
	}
}
