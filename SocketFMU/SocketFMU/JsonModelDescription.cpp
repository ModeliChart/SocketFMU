#include "JsonModelDescription.h"
#include "SocketChannel.h"
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"
#include <fstream>

namespace pt = boost::property_tree;

void JsonModelDescription::Load(std::string resourceDir)
{
	// Load the json	
	pt::ptree json;
	pt::read_json(resourceDir + "/modelDescription.json", json);

	// Parse JSON
	Guid = json.get<std::string>("Guid");
	Protocol = json.get<std::string>("Protocol");
	RemoteAddress = json.get<std::string>("RemoteAddress");
	ReceivePort = json.get<int>("ReceivePort");
	SendPort = json.get<int>("SendPort");

	// Iterate over channel nodes
	for (auto& channel : json.get_child("Channels"))
	{
		// Create and add channel
		Channels.push_back(SocketChannel(
			channel.second.get<std::string>("Name"),
			channel.second.get<unsigned int>("ValueRef"),
			channel.second.get<bool>("IsSender"),
			channel.second.get<bool>("ReverseEndianness")));
	}
}
