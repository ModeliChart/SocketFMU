#include "ConnectionBase.h"


ConnectionBase::ConnectionBase():
	_valuesReceivedCallback(NULL), _logger(NULL), _receivedValuesCount(0)
{
}

void ConnectionBase::SetCallbacks(ValuesReceivedFn callback, LoggerFn logger)
{
	_valuesReceivedCallback = callback;
	_logger = logger;
}

void ConnectionBase::SetReceivedValuesCount(int count)
{
	_receivedValuesCount = count;
}
