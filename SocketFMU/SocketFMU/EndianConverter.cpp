#include "EndianConverter.h"
#include <cstdint>
#include <cstring>

// Be able to check endianess at runtime
const int endianess = 1;
#define is_bigendian() ( (*(char*)&endianess) == 0 )

EndianConverter::EndianConverter()
{
	// TODO Auto-generated constructor stub

}
EndianConverter::~EndianConverter()
{
	// TODO Auto-generated destructor stub
}

// Converts Host <-> Network if necessary, works bidirectional
void EndianConverter::ReverseEndianness(int32_t &value)
{
	if (is_bigendian())
	{
		// nothing to do
		return;
	}
	char in[4], out[4];
	memcpy(in, &value, 4);
	out[0] = in[3];
	out[1] = in[2];
	out[2] = in[1];
	out[3] = in[0];
	memcpy(&value, out, 4);
}
// Converts Host <-> Network if necessary, works bidirectional
void EndianConverter::ReverseEndianness(uint32_t &value)
{
	if (is_bigendian())
	{
		// nothing to do
		return;
	}
	char in[4], out[4];
	memcpy(in, &value, 4);
	out[0] = in[3];
	out[1] = in[2];
	out[2] = in[1];
	out[3] = in[0];
	memcpy(&value, out, 4);
}
// Converts Host <-> Network if necessary, works bidirectional
void EndianConverter::ReverseEndianness(float &value)
{
	if (is_bigendian())
	{
		// nothing to do
		return;
	}
	char in[4], out[4];
	memcpy(in, &value, 4);
	out[0] = in[3];
	out[1] = in[2];
	out[2] = in[1];
	out[3] = in[0];
	memcpy(&value, out, 4);
}
// Converts Host <-> Network if necessary, works bidirectional
void EndianConverter::ReverseEndianness(double &value)
{
	if (is_bigendian())
	{
		// nothing to do
		return;
	}
	char in[8], out[8];
	memcpy(in, &value, 8);
	out[0] = in[7];
	out[1] = in[6];
	out[2] = in[5];
	out[3] = in[4];
	out[4] = in[3];
	out[5] = in[2];
	out[6] = in[1];
	out[7] = in[0];
	memcpy(&value, out, 8);
}

void EndianConverter::ReverseEndianness(std::vector<uint32_t> &value)
{
	// If we run on Big Endian machine -> Host == Network Order
	if (is_bigendian())
	{
		return;
	}
	for (unsigned int i = 0; i < value.size(); i++)
	{
		EndianConverter::ReverseEndianness(value[i]);
	}
}
void EndianConverter::ReverseEndianness(std::vector<int32_t> &value)
{
	// If we run on Big Endian machine -> Host == Network Order
	if (is_bigendian())
	{
		return;
	}
	for (unsigned int i = 0; i < value.size(); i++)
	{
		// ntohl only takes care of the byte order so our sign will not be changed
		EndianConverter::ReverseEndianness(value[i]);
	}
}
void EndianConverter::ReverseEndianness(std::vector<double>& value)
{
	// If we run on Big Endian machine -> Host == Network Order
	if (is_bigendian())
	{
		return;
	}
	for (unsigned int i = 0; i < value.size(); i++)
	{
		EndianConverter::ReverseEndianness(value[i]);
	}
}

