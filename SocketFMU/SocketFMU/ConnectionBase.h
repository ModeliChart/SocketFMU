#pragma once
#include <vector>
#include <functional>
#include <memory>
#include "fmi2FunctionTypes.h"

typedef std::function<void(std::shared_ptr<std::vector<double>> values)> ValuesReceivedFn;
typedef std::function<void(fmi2Status status, std::string message)> LoggerFn;


class ConnectionBase
{
private:

protected:
	// Call this function when new values arrived
	ValuesReceivedFn _valuesReceivedCallback;
	LoggerFn _logger;
	int _receivedValuesCount;
public:
	ConnectionBase();
	virtual ~ConnectionBase() {};

	/**
	* Start listening to the receivePort.
	* The sendPort will be used after the client has connected.
	*/
	virtual fmi2Status Start(std::string remoteAddress, int receivePort, int sendPort) = 0;

	/**
	* Stop the receiver task and close the connection.
	*/
	virtual void Stop() = 0;

	/**
	* The callabck is executed when new values have arrived.
	*/
	void SetCallbacks(ValuesReceivedFn callback, LoggerFn logger);

	/**
	* The count of values that will be received via the connection.
	*/
	void SetReceivedValuesCount(int count);

	/**
	* Send the given values to the connected client.
	* \param values The values will be sent. Should be {timestamp, values}
	*/
	virtual fmi2Status SendValues(const std::vector<double>& values) = 0;
};

// TimIstToll<3

