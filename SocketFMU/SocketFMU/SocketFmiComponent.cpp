#include "SocketFmiComponent.h"
#include "SocketChannel.h"
#include "TcpConnection.h"
#include "UdpConnection.h"
#include "JsonModelDescription.h"
#include "EndianConverter.h"

SocketFmiComponent::SocketFmiComponent()
{
	// We are fine at start
	_status = fmi2OK;
}

SocketFmiComponent::~SocketFmiComponent()
{

}

fmi2Status SocketFmiComponent::Instantiate(std::string instanceName, std::string xmlGuid, std::string resourceDir,
										   const fmi2CallbackFunctions * callbackFunctions)
{
	// We need the callbackFunctions for many other operations so store them first
	_callbackFunctions = callbackFunctions;
	_instanceName = instanceName;
	_guid = xmlGuid;

	// Gather data from json
	JsonModelDescription json;
	try
	{
		json.Load(resourceDir);
	}
	catch (std::runtime_error err)
	{
		Log(fmi2Error, "Failed loading the config, message: " + std::string(err.what()));
	}
	if (xmlGuid.compare(json.Guid) != 0)
	{
		Log(fmi2Error, "The GUID of the json does not math the instantiation parameter");
		return fmi2Error;
	}
	// Copy and sort the channels by valueRef
	_channels = json.Channels;
	std::sort(_channels.begin(), _channels.end(),
			  [](SocketChannel a, SocketChannel b) { return a.GetValueRef() < b.GetValueRef(); });

	// Check the protocol
	if (json.Protocol == "TCP")
	{
		_connection = std::make_unique<TcpConnection>();
	}
	else if (json.Protocol == "UDP")
	{
		_connection = std::make_unique<UdpConnection>();
	}
	else
	{
		Log(fmi2Error, "Unknown Protocol.");
		return fmi2Error;
	}

	// Setup the connection
	int receiverChannelsCount = 0;
	for (auto c : _channels)
	{
		if (!c.IsSender())
		{
			receiverChannelsCount++;
		}
	}
	// Setup the connection
	_connection->SetReceivedValuesCount(receiverChannelsCount);
	_connection->SetCallbacks(
		std::bind(&SocketFmiComponent::valuesReceivedCallback, this, std::placeholders::_1),
		std::bind(&SocketFmiComponent::Log, this, std::placeholders::_1, std::placeholders::_2));
	_connection->Start(json.RemoteAddress, json.ReceivePort, json.SendPort);

	return fmi2OK;
}

void SocketFmiComponent::valuesReceivedCallback(std::shared_ptr<std::vector<double>> receivedValues)
{
	// Iterators
	size_t receivedValue = 0;
	size_t channel = 0;
	_mutex.lock();
	// received values and channels are sorted by valueRef
	 
	//Log(fmi2Status::fmi2Error, std::string("receivedValues size ") + std::to_string(receivedValues->size()) + std::string("  _channels.size() ") + std::to_string(_channels.size()));
	while (receivedValue < receivedValues->size() && channel < _channels.size())
	{
		bool foundRightChannel = false;
		while (!foundRightChannel && channel < _channels.size())
		{
			// Break condition if found -> setvalue
			if (!_channels[channel].IsSender())
			{
				if (_channels[channel].ReverseEndianness())
				{
					EndianConverter::ReverseEndianness((*receivedValues)[receivedValue]);
				}				
				_channels[channel].SetValue((*receivedValues)[receivedValue]);
				foundRightChannel = true;
			}
			channel++;
		}
		receivedValue++;
	}
	_mutex.unlock();
}

void SocketFmiComponent::DoStep(fmi2Real currentCommunicationPoint, fmi2Real stepSize)
{
	// Collect timestamp + all values in one buffer
	std::vector<double> buff;
	// currentCommunicationPoint is used as timestamp to stay in sync with the frontend
	buff.push_back(currentCommunicationPoint);
	_mutex.lock();
	for (SocketChannel channel : _channels)
	{
		if (channel.IsSender())
		{
			double val = channel.GetValue();
			if (channel.ReverseEndianness())
			{
				EndianConverter::ReverseEndianness(val);
			}
			buff.push_back(val);
		}
	}
	_mutex.unlock();

	_connection->SendValues(buff);
	_currentTime += stepSize;
}

void SocketFmiComponent::SetValues(const fmi2ValueReference vr[], const fmi2Real values[], size_t nvr)
{
	_mutex.lock();
	// Iterate the vrs
	for (size_t i = 0; i < nvr; i++)
	{
		// The channel is at the position of vr
		if (_channels[vr[i]].IsSender())
		{
			_channels[vr[i]].SetValue(values[i]);
		}
	}
	_mutex.unlock();
}

void SocketFmiComponent::GetValues(const fmi2ValueReference vr[], fmi2Real values[], size_t nvr)
{
	_mutex.lock();
	for (size_t i = 0; i < nvr; i++)
	{
		// The channel is at the position of vr
		values[i] = _channels[vr[i]].GetValue();
	}
	_mutex.unlock();
}

fmi2Status SocketFmiComponent::GetStatus()
{
	return _status;
}

void SocketFmiComponent::Log(fmi2Status status, std::string msg)
{
	_callbackFunctions->logger(_callbackFunctions->componentEnvironment, _instanceName.c_str(), status, "category", msg.c_str());
}

void SocketFmiComponent::Reset()
{
	// Reset values
	_currentTime = 0;
	for (SocketChannel channel : _channels)
	{
		channel.SetValue(0);
	}
	_status = fmi2OK;
}

void SocketFmiComponent::Terminate()
{
	_connection->Stop();
}
