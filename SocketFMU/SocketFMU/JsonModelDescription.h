#pragma once
#include <string>
#include <vector>

// Forward declare
class SocketChannel;

struct JsonModelDescription
{
	void Load(std::string resourcesDir);
	std::string Guid;
	std::string Protocol;
	std::string RemoteAddress;
	int ReceivePort;
	int SendPort;
	bool UseDebugDll;
	std::vector<SocketChannel> Channels;
};

