// Implementiert die zu exportierrenden fmi2Functions
#include "fmi2Functions.h"
#include "SocketFmiComponent.h"
#include <string>
#include <cstring>

// NOTE ValueRef: The Index begin at 0. Every number must be used: 0,1,2,3,...
// ValueRef matches the ones from the json and xml

// Return the TypesPlatform from "fmi2TypesPlatform.h"
const char* fmi2GetTypesPlatform()
{
	return fmi2TypesPlatform;
}
// Return the Version 2.0
const char* fmi2GetVersion()
{
	return "2.0";
}


fmi2Status fmi2SetDebugLogging(fmi2Component c, fmi2Boolean loggingOn, size_t nCategories, const fmi2String categories[])
{
	return fmi2Warning;
}
fmi2Status fmi2GetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Real value[])
{
	if (c != nullptr)
	{
		SocketFmiComponent* comp = (SocketFmiComponent*)c;
		comp->GetValues(vr, value, nvr);
		return fmi2OK;
	}
	else
	{
		return fmi2Error;
	}
}
fmi2Status fmi2GetInteger(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Integer value[])
{
	return fmi2OK;
}
fmi2Status fmi2GetBoolean(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Boolean value[])
{
	return fmi2OK;
}
fmi2Status fmi2GetString(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2String  value[])
{
	return fmi2OK;
}

fmi2Status fmi2SetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Real value[])
{
	if (c != nullptr)
	{
		SocketFmiComponent* comp = (SocketFmiComponent*)c;
		comp->SetValues(vr, value, nvr);
		return fmi2OK;
	}
	else
	{
		return fmi2Error;
	}
}
fmi2Status fmi2SetInteger(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Integer value[])
{
	return fmi2OK;
}
fmi2Status fmi2SetBoolean(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Boolean value[])
{
	return fmi2OK;
}
fmi2Status fmi2SetString(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2String  value[])
{
	return fmi2OK;
}

fmi2Status fmi2GetFMUstate(fmi2Component c, fmi2FMUstate* FMUstate)
{
	return fmi2Warning;
}
fmi2Status fmi2SetFMUstate(fmi2Component c, fmi2FMUstate FMUstate)
{
	return fmi2Warning;
}
fmi2Status fmi2FreeFMUstate(fmi2Component c, fmi2FMUstate* FMUstate)
{
	return fmi2Warning;
}
fmi2Status fmi2SerializedFMUstateSize(fmi2Component c, fmi2FMUstate FMUstate, size_t *size)
{
	return fmi2Warning;
}
fmi2Status fmi2SerializeFMUstate(fmi2Component c, fmi2FMUstate FMUstate, fmi2Byte serializedState[], size_t size)
{
	return fmi2Warning;
}
fmi2Status fmi2DeSerializeFMUstate(fmi2Component c, const fmi2Byte serializedState[], size_t size, fmi2FMUstate* FMUstate)
{
	return fmi2Warning;
}
fmi2Status fmi2GetDirectionalDerivative(fmi2Component c, const fmi2ValueReference z_ref[], size_t nz,
	const fmi2ValueReference v_ref[], size_t nv, const fmi2Real dv[], fmi2Real dz[])
{
	return fmi2Warning;
}

fmi2Component fmi2Instantiate(fmi2String instanceName, fmi2Type fmuType, fmi2String fmuGUID,
	fmi2String fmuResourceLocation, const fmi2CallbackFunctions* functions,
	fmi2Boolean visible, fmi2Boolean loggingOn)
{
	// Only CoSimulation is supported
	if (fmuType != fmi2CoSimulation)
	{
		functions->logger(nullptr, instanceName, fmi2Error, "category", "The fmu only supports CoSimulation mode.");
		return nullptr;
	}

	// Create path to config file
	//TODO parse the resourcelocation as uri
	std::string resourceUri(fmuResourceLocation);
	std::string fIdent("file:///");
	std::string fIdentS("file:/");
	// Find the file identifier at the begin
	if (resourceUri.find(fIdent) == 0)
	{
		resourceUri = resourceUri.substr(fIdent.length(), resourceUri.length() - fIdent.length());
	}
	else if (resourceUri.find(fIdentS) == 0)
	{
		resourceUri = resourceUri.substr(fIdentS.length(), resourceUri.length() - fIdentS.length());
	}
	else
	{
		// The resUri is non valid
		functions->logger(nullptr, instanceName, fmi2Error, "category", "The fmuResourceLocation is not a valid Uri.");
		return nullptr;
	}

	// Init the SocketComponent
	SocketFmiComponent* c = new SocketFmiComponent();
	fmi2Status instantiateResult = c->Instantiate(instanceName, fmuGUID, resourceUri, functions);
	if (instantiateResult == fmi2Error || instantiateResult == fmi2Fatal)
	{
		// Error message handled in the instantiate method
		return nullptr;
	}

	// When everything is okay the component is returned
	return c;
}

void fmi2FreeInstance(fmi2Component c)
{
	if (c != nullptr)
	{
		SocketFmiComponent* comp = (SocketFmiComponent*)c;
		delete(comp);
	}
}
fmi2Status fmi2SetupExperiment(fmi2Component c, fmi2Boolean relativeToleranceDefined, fmi2Real relativeTolerance,
	fmi2Real tStart, fmi2Boolean tStopDefined, fmi2Real tStop)
{
	// This fmu runs coninously so nothing can be set here
	return fmi2OK;
}
fmi2Status fmi2EnterInitializationMode(fmi2Component c)
{
	// Nothing to do here
	return fmi2OK;
}
fmi2Status fmi2ExitInitializationMode(fmi2Component c)
{
	// Nothing to do here
	return fmi2OK;
}
fmi2Status fmi2Terminate(fmi2Component c)
{
	if (c != nullptr)
	{
		SocketFmiComponent* comp = (SocketFmiComponent*)c;
		comp->Terminate();
		return fmi2OK;
	}
	else
	{
		return fmi2Error;
	}
}
fmi2Status fmi2Reset(fmi2Component c)
{
	if (c != nullptr)
	{
		SocketFmiComponent* comp = (SocketFmiComponent*)c;
		comp->Reset();
		return fmi2OK;
	}
	else
	{
		return fmi2Error;
	}
}


/***************************************************
Functions for FMI for Co-Simulation
****************************************************/

fmi2Status fmi2SetRealInputDerivatives(fmi2Component c, const  fmi2ValueReference vr[],
	size_t nvr, const  fmi2Integer order[], const  fmi2Real value[])
{
	return fmi2Warning;
}
fmi2Status fmi2GetRealOutputDerivatives(fmi2Component c, const   fmi2ValueReference vr[],
	size_t  nvr, const   fmi2Integer order[], fmi2Real value[])
{
	return fmi2Warning;
}
fmi2Status fmi2DoStep(fmi2Component c, fmi2Real currentCommunicationPoint, fmi2Real
	communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPoint)
{
	if (c != nullptr)
	{
		SocketFmiComponent* comp = (SocketFmiComponent*)c;
		// DoStep will send the latest values
		comp->DoStep(currentCommunicationPoint, communicationStepSize);
		return fmi2OK;
	}
	else
	{
		return fmi2Error;
	}
}
fmi2Status fmi2CancelStep(fmi2Component c)
{
	return fmi2Warning;
}
fmi2Status fmi2GetStatus(fmi2Component c, const fmi2StatusKind s, fmi2Status*  value)
{
	if (c != nullptr)
	{
		fmi2Status status = ((SocketFmiComponent*)c)->GetStatus();
		std::memcpy(value, &status, sizeof(status));
		return fmi2OK;
	}
	else
	{
		return fmi2Error;
	}
}
fmi2Status fmi2GetRealStatus(fmi2Component c, const fmi2StatusKind s, fmi2Real*    value)
{
	return fmi2Warning;
}
fmi2Status fmi2GetIntegerStatus(fmi2Component c, const fmi2StatusKind s, fmi2Integer* value)
{
	return fmi2Warning;
}
fmi2Status fmi2GetBooleanStatus(fmi2Component c, const fmi2StatusKind s, fmi2Boolean* value)
{
	return fmi2Warning;
}
fmi2Status fmi2GetStringStatus(fmi2Component c, const fmi2StatusKind s, fmi2String*  value)
{
	return fmi2Warning;
}