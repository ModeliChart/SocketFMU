#pragma once
#include <vector>
#include <stdint.h>

class EndianConverter
{
public:
	EndianConverter();
	virtual ~EndianConverter();

	std::vector<double> test;

	/// Converts Host <-> Network if necessary, works bidirectional
	static void ReverseEndianness(int32_t& value);
	/// Converts Host <-> Network if necessary, works bidirectional
	static void ReverseEndianness(uint32_t& value);
	/// Converts Host <-> Network if necessary, works bidirectional
	static void ReverseEndianness(float& value);
	/// Converts Host <-> Network if necessary, works bidirectional
	static void ReverseEndianness(double& value);

	/// Converts Host for elements of a vector <-> Network if necessary, works bidirectional
	static void ReverseEndianness(std::vector<uint32_t>& value);
	/// Converts Host for elements of a vector <-> Network if necessary, works bidirectional
	static void ReverseEndianness(std::vector<int32_t>& value);
	/// Converts Host for elements of a vector <-> Network if necessary, works bidirectional
	static void ReverseEndianness(std::vector<double>& value);
};
