#pragma once
#include <thread>
#include "ConnectionBase.h"
#include "boost/asio.hpp"

namespace asio = boost::asio;
using udp = asio::ip::udp;

class UdpConnection : public ConnectionBase
{
private:
	// Callbacks
	const fmi2CallbackFunctions * _callbacks;
	// async execution
	std::thread _ioThread;
	asio::io_service _ioService;
	std::unique_ptr<asio::io_service::work> _work;
	// Socket
	udp::socket _sendSocket;
	udp::socket _receiveSocket;

	void startReceive();

	/**
	* Callback for the async send operation
	* \param buffer Bind the buffer to the handler to keep it alive until the operation has finished.
	*/
	void handleSend(const boost::system::error_code & error, std::size_t bytes_transferred, std::shared_ptr<std::vector<double>> buffer);
	/**
	* Callback for the async receive operation
	* \param buffer Bind the buffer to the handler to keep it alive until the operation has finished.
	*/
	void handleReceive(const boost::system::error_code & error, std::size_t byte_transferred,
		std::shared_ptr<std::vector<double>> buffer, std::shared_ptr<udp::endpoint> endpoint);

public:
	UdpConnection();
	~UdpConnection();

	fmi2Status Start(std::string remoteAddress, int receivePort, int sendPort) override;
	void Stop();
	fmi2Status SendValues(const std::vector<double>& values) override;
};