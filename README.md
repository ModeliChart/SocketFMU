# SocketFMU
## Introduction
 This FMU uses Sockets (TCP or UDP) for sharing data. The FMU serves as data server. 
 
 Input variables will be sent to all clients on DoStep. 
 
 Output variables can only be received from the first client and will be updated on change.
 
## Build instruction
 ### SocketFMU
 Boost precompiled libraries are needed [for Windows](https://sourceforge.net/projects/boost/files/boost-binaries/). Currently, Boost 1.74.0 for MSVC 14.1 is used. The project is configured that Boost sits at C:\local\boost_1_74_0 (the default install directory).

 ### SocketFMU_Generator
 The project expects SocketFMU\bin\x64\Release\SocketFMU.dll and SocketFmuLinux\bin\x64\Release\SocketFmuLinux.so to be existent. So those projects need to be built first or, if they can not be built, a mockup dll/so file needs to be provided (the generator just copies the dll/so in the FMU). 
## FMU Generator
 Creates a FMU which contains the dll and xml. 
 Additionally a json is includeded in the resources folder which will be loaded by the dll and the generator tool to create the channels.
 
 Additionally all neccessary dlls must be included in the same folder as the model.dll.
 
 The config file will be stored in the resources folder as a json. 
 
 Json is used for easier encoding and decoding with boost. JSON is lightwheight compared to .xml.